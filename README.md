Primeiro passo para criar o projeto:

````
npx create-react-app aulafront
````

Esse comando irá criar uma pasta com este nome, sendo este o novo projeto. Se a mensagem final for algo como `Happy hacking!` quer dizer deu certo :), caso contrário, reveja as instalações do npm

Existem outras formas de criar o projeto, algumas usam Vite ou Snowpack, estes caras criam um projeto com react, mas utilizando ECMA Script Modules facilitando as importações entre arquivos JS, enquanto o create-react-app mantém com o webpack controlando de outra forma, pois navegadores antigos não suportam ECMA.
Para criar projeto acom Vite, por exemplo, pode-se usar o comando abaixo, porém o proojeto atual não está usando TS e vamos usar o create-react-app.

```
yarn create vite aulafrontvite --template react-ts
```

Após isso, já será possível entrar na pasta criada e iniciar os testes no projeto, dentro desta pasta existirão alguns diretórios:

```
+ aulafont
---+ node_modules/ #Pasta das bibliotecas externas do JS
---+ public/ #arquivos de imagem, js e outros que você poderá acessar exportando
---+ src/ # código do projeto
package-lock.json #arquivo criado ao executar o npx
package.json #arquivo com metadados sobre as dependências do projeto
````

Os comandos podem ser utilizados para rodar ou fazer build do projeto como:
```npm run build```
Que irá gerar uma pasta chamada build com o projeto pronto para ser colocado no ar

Para inciar a programar e fazer o projeto é bom startar e ver como as coisas estão indo, para isso use
```npm start```
Este comando irá abrir a porta 3000 da sua máquina local para visualizar no navegador.

O processo se inicia através do arquivo index.html presente na pasta public

No index.js, que é acionado em seguida, ocorre a inserção do elemento html na div root

